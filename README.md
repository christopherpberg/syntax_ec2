syntax_ec2
=========

Provisions:		
- AWS EC2 Instance w/ Ubuntu 18.04
- Security Group w/ Inbound & Outbound rules 

Requirements
------------
- aws_access_key & aws_secret_key must be valid.
- ec2.keyname must exist in AWS  


Role Variables
--------------
- aws_access_key 
- aws_secret_key 
- ec2 - Description of ec2 instance 
- sg - Description of security group & rules
Dependencies
------------

- python >= 2.6
- boto

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: awscli
      roles:
     - { role: syntax_ec2, aws_access_key: "${AWS_ACCESS_KEY}", aws_secret_key: "${AWS_SECRET_KEY}", ec2.keyname: ${my-aws-access-key} }

License
-------

GNU

See also
------------------
N/A
